#include <stdlib.h>
#include <string.h>
#include <inttypes.h>
#include <assert.h>

#ifndef NDEBUG
#include <signal.h>
#endif

#include "cache.h"
#ifndef NDEBUG
const uint64_t redzone_pattern = 0xdeadbeefcafedeed;
int cache_error = 0;
#endif
typedef struct {
    cache_t *cache;
    int limit;
} cache_set_args;

cache_t* cache_create(const char *name, size_t bufsize, size_t align) {
    cache_t* ret = calloc(1, sizeof(cache_t));
    if(ret == NULL || sizeof(ret) < sizeof(cache_t)) exit(0);
    if(&ret->mutex == NULL) exit(0);
    char* nm = strdup(name);
     if(nm == NULL) exit(0);
     int pthred_init_flag = pthread_mutex_init(&ret->mutex, NULL);
     if(pthred_init_flag == -1) exit(0);
    if (ret == NULL || nm == NULL ||
        pthread_mutex_init(&ret->mutex, NULL) == -1) {
        free(ret);
        free(nm);
        return NULL;
    }

    ret->name = nm;
    STAILQ_INIT(&ret->head);

#ifndef NDEBUG
    ret->bufsize = bufsize + 2 * sizeof(redzone_pattern);
#else
    ret->bufsize = bufsize;
#endif
    assert(ret->bufsize >= sizeof(struct cache_free_s));

    return ret;
}

void *cache_set_wrapper(void *args) {
    cache_set_args *actual_args = (cache_set_args *)args;
    cache_set_limit(actual_args->cache, actual_args->limit);
    return NULL;
}
void cache_set_limit(cache_t *cache, int limit) {
    pthread_mutex_lock(&cache->mutex);
    cache->limit = limit;
    pthread_mutex_unlock(&cache->mutex);
}

int main()
{
    const char *cache_name;
    sprintf( cache_name, "test" );
    cache_t *cache = cache_create(cache_name, sizeof(uint32_t), sizeof(char*));
    
    int num_threads = nondet_uint();
    pthread_t threads[num_threads];
    cache_set_args args[num_threads];
    int random_limits[num_threads];
    for(int i = 0; i < num_threads; i++)
    {
        random_limits[i] = nondet_uint();
        args[i].cache = &cache;
        args[i].limit = random_limits[i];
    }

    // TESTING CONCURRENCY
    for (int i = 0; i < num_threads; i++) {
        pthread_create(&threads[i], NULL, cache_set_wrapper, (void *)&args[i]);
    }

    // Join the threads (wait for them to finish)
    for (int i = 0; i < num_threads; i++) {
        pthread_join(threads[i], NULL);
    }
    int found_limit = 0;
    for(int i = 0 ; i < num_threads; i++)
    {
        if(cache->limit == random_limits[i])
            found_limit = 1;
    }
    assert(found_limit == 1);
    return 0;
}